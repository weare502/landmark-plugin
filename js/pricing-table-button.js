(function() {
	tinymce.PluginManager.add('lnb_pricing_table_button', function( editor, url ) {
		editor.addButton('lnb_pricing_table_button', {
			text: 'Pricing Table',
			icon: false,
			onclick: function() {
				editor.insertContent('[landmark-pricing-table]');
			}
		});
	});
})();