<?php

class Landmark_Other_Pages extends WP_Widget {

	function __construct() {
			// Instantiate the parent object
			parent::__construct( 
				'landmark_other_pages', // Base ID
				__( 'Landmark: Other Pages in this Section', 'lnb' ), // Name
				array( 
					'description' => __( 'Show a list of child / sibling pages relevant to the current page.', 'lnb' ), 
				)
			);
		}


	function widget( $args, $instance ) {
		global $post;

		$title = '';
		$parent = '';

		if ( have_rows( 'additional_services_links' ) ){
			// we already have links on the page
			return false; 
		} elseif ( !empty( $post->post_parent ) ){ 
			// if this is a sub page
			$title = __('Other Pages in this Section', 'lnb' );
			$parent = $post->post_parent;
		} else if ( 0 != count( get_pages('child_of='.$post->ID) ) ){
			// if this is a parent page with children
			$title = __('Pages in this Section', 'lnb' );
			$parent = $post->ID;
		} else {
			// don't show a list for pages without children
			return false;
		}

		$pages = wp_list_pages( array( 
				'child_of' => $parent,
				'echo' => false,
				'title_li' => null
			) );

		echo $args['before_widget'];

		echo $args['before_title'] . $title . $args['after_title'];
		
		echo $pages;

		echo $args['after_widget'];

	}


}

add_action( 'widgets_init', function() {

	register_widget( 'Landmark_Other_Pages' );

});