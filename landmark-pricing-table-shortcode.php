<?php


add_shortcode( 'landmark-pricing-table', function(){


	// CHECK IF THE CURRENT PAGE HAS A PRICING TABLE ENABLED
	if ( false == get_field('show_pricing_table') ){
		return false;
	}

	// SET THE GLOBAL SO WE CAN DO STUFF
	global $post;

	ob_start();

	$table_name = get_field('table_name');

	if ( have_rows( 'pricing_table') ) : ?>

	<div class="show-mobile"><?php _e( 'Scroll Right to See More', 'lnb' ); ?>&nbsp;<span class="fa fa-caret-right"></span></div>

	<div class="pricing-table" id="pricing-table-<?php echo get_the_id(); ?>">

		<?php if ( !empty( $table_name ) ) : ?>
			<h2><?php echo $table_name; ?></h2>
		<?php endif; ?>

		<table summary="Pricing Table">

			<thead>

				<tr>

					<th><?php _e( 'Account Name', 'lnb' ); ?></th>
					<th><?php _e( 'Opening Balance', 'lnb' ); ?></th>
					<th><?php _e( 'Earns Interest', 'lnb' ); ?></th>
					<th><?php _e( 'Minimum Balance', 'lnb' ); ?></th>
					<th><?php _e( 'Monthly Service Charge', 'lnb' ); ?></th>
				
				</tr>

			</thead>

			<tbody>
		
			<?php while ( have_rows( 'pricing_table' ) ) : the_row(); ?>

				<tr>
					
					<td><?php the_sub_field('account_name'); ?></td>
					<td><?php $num = intval( get_sub_field('opening_balance') ) ? '$' : ''; ?><?php echo $num; the_sub_field('opening_balance'); ?></td>
					<td><?php the_sub_field('earns_interest'); ?></td>
					<td><?php $num = intval( get_sub_field('minimum_balance') ) || get_sub_field('minimum_balance') == '0' ? '$' : ''; ?><?php echo $num;the_sub_field('minimum_balance'); ?></td>
					<td><?php the_sub_field('monthly_service_charge'); ?></td>
				
				</tr>

			<?php endwhile; ?>

			</tbody>

		</table>

	</div>

	<?php else : 
	
		return false;
	
	endif;

	return ob_get_clean();

} );


function lnb_add_pricing_table_mce_button() {
    // check user permissions
    if ( !current_user_can( 'edit_posts' ) && !current_user_can( 'edit_pages' ) ) {
        return;
    }
    // check if WYSIWYG is enabled
    if ( 'true' == get_user_option( 'rich_editing' ) ) {
        add_filter( 'mce_external_plugins', function( $plugin_array ){

            $plugin_array['lnb_pricing_table_button'] = plugins_url( 'js/pricing-table-button.js', __FILE__ );
            return $plugin_array;

        } );

        add_filter( 'mce_buttons', function( $buttons ){

            array_push( $buttons, 'lnb_pricing_table_button' );
            return $buttons;

        } );
    }
}
add_action('admin_head', 'lnb_add_pricing_table_mce_button');








