<?php

add_shortcode( 'landmark-card-table', function(){


	// CHECK IF THE CURRENT PAGE HAS A PRICING TABLE ENABLED
	if ( false == get_field('show_card_table') ){
		return false;
	}

	// SET THE GLOBAL SO WE CAN DO STUFF
	global $post;

	ob_start();

	$i = 0;

	if ( have_rows( 'card_comparison') ) : ?>

	<div class="show-mobile"><?php _e( 'Scroll Right to See More', 'lnb' ); ?>&nbsp;<span class="fa fa-caret-right"></span></div>

	<div class="card-table" id="card-table-<?php echo get_the_id(); ?>">

		<table summary="Card Comparison Chart">

			<thead>

				<tr>

					<th>&nbsp;</th>
		
			<?php while ( have_rows( 'card_comparison' ) ) : the_row(); ?>
					
					<th><?php the_sub_field('card_name'); ?></th>

			<?php endwhile; ?>

				</tr>

			</thead>

			<tbody>

				<tr>
					
					<th><?php _e( 'Cash Withdrawals Allowed', 'lnb' ); ?></th>

					<?php while ( have_rows( 'card_comparison' ) ) : the_row(); ?>
							
							<td><?php the_sub_field('cash_withdrawals'); ?></td>

					<?php endwhile; ?>

				</tr>

				<tr>
					
					<th><?php _e( 'Load Amount', 'lnb' ); ?></th>

					<?php while ( have_rows( 'card_comparison' ) ) : the_row(); ?>
							
							<td><?php the_sub_field('load_amounts'); ?></td>

					<?php endwhile; ?>

				</tr>

				<tr>
					
					<th><?php _e( 'Reloadable', 'lnb' ); ?></th>

					<?php while ( have_rows( 'card_comparison' ) ) : the_row(); ?>
							
							<td><?php the_sub_field('reloadable'); ?></td>

					<?php endwhile; ?>

				</tr>

				<tr>
					
					<th><?php _e( 'Purchase Fee', 'lnb' ); ?></th>

					<?php while ( have_rows( 'card_comparison' ) ) : the_row(); ?>
							
							<td><?php the_sub_field('purchase_fee'); ?></td>

					<?php endwhile; ?>

				</tr>

				<tr>
					
					<th><?php _e( 'Lost/Stolen Card Replacement Fee', 'lnb' ); ?></th>

					<?php while ( have_rows( 'card_comparison' ) ) : the_row(); ?>
							
							<td><?php the_sub_field('card_replacement_fee'); ?></td>

					<?php endwhile; ?>

				</tr>

				<tr>
					
					<th><?php _e( 'Inactivity Fee', 'lnb' ); ?></th>

					<?php while ( have_rows( 'card_comparison' ) ) : the_row(); ?>
							
							<td><?php the_sub_field('inactivity_fee'); ?></td>

					<?php endwhile; ?>

				</tr>

				<tr>
					
					<th><?php _e( 'Companion Card', 'lnb' ); ?></th>

					<?php while ( have_rows( 'card_comparison' ) ) : the_row(); ?>
							
							<td><?php the_sub_field('companion_card'); ?></td>

					<?php endwhile; ?>

				</tr>

			</tbody>

		</table>

	</div>

	<?php else : 
	
		return false;
	
	endif;

	return ob_get_clean();

} );