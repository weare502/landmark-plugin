<?php
/**
 * Plugin Name: Landmark Plugin
 * Description: Adds features and functionality to this website. Do not delete or deactivate!
 * Version: 0.1
 * Plugin Author: 502 Media Group
 */



// Quick Links Widget
include 'quick-links-widget.php';

include 'other-pages-widget.php';

include 'landmark-pricing-table-shortcode.php';

include 'landmark-card-table-shortcode.php';


// Careers Form Stuff
add_filter( 'gform_pre_render_1', 'populate_checkbox' );
add_filter( 'gform_pre_validation_1', 'populate_checkbox' );
add_filter( 'gform_pre_submission_filter_1', 'populate_checkbox' );
add_filter( 'gform_admin_pre_render_1', 'populate_checkbox' );
function populate_checkbox( $form ) {

    if ( is_admin() )
        return $form;

    foreach( $form['fields'] as $field )  {

        //NOTE: replace 3 with your checkbox field id
        $field_id = 1;
        if ( $field->id != $field_id ) {
            continue;
        }

        // you can add additional parameters here to alter the posts that are retreieved
        // more info: [http://codex.wordpress.org/Template_Tags/get_posts](http://codex.wordpress.org/Template_Tags/get_posts)
        $locations = isset( $_GET['locations'] ) ? $_GET['locations'] : false ;

        natcasesort( $locations );

        if ( false === $locations )
        	continue;

        $input_id = 1;

        foreach( $locations as $location ) {

            //skipping index that are multiples of 10 (multiples of 10 create problems as the input IDs)
            if ( $input_id % 10 == 0 ) {
                $input_id++;
            }

            $choices[] = array( 'text' => $location, 'value' => $location );
            $inputs[] = array( 'label' => $location, 'id' => "{$field_id}.{$input_id}" );

            $input_id++;
        }

        $field->choices = $choices;
        $field->inputs = $inputs;

    }

    return $form;
}

// Hide Simple Locator Fields
add_action('admin_head', function(){ ?>

	<style type="text/css">
		#wpsl-meta-box .half, #wpsl-meta-box .full:last-child {
			display: none;
		}

       #dashboard_right_now li .location-count:before {
            content: '\f513';
       }
	</style>


<?php });

add_action( 'after_setup_theme' , function(){

    if( function_exists('acf_add_options_page') ) {

        acf_add_options_page(array(
            'page_title'    => 'Landmark Theme Settings',
            'menu_title'    => 'Theme Settings',
            'menu_slug'     => 'theme-general-settings',
            'capability'    => 'edit_posts',
            'redirect'      => false
        ));

    }

} );


add_filter( 'mce_buttons_2', function( $buttons ){

    array_unshift( $buttons, 'styleselect' );
    return $buttons;

} );

add_filter( 'tiny_mce_before_init', function( $settings ){

        $style_formats = array(
            array(
                'title' => 'External Link Level 1',
                'selector' => 'a',
                'classes' => 'external-lvl-1'
            ),
            array(
                'title' => 'External Link Level 2',
                'selector' => 'a',
                'classes' => 'external-lvl-2'
            ),
            array(
                'title' => 'External Link Level 3',
                'selector' => 'a',
                'classes' => 'external-lvl-3'
               )
        );

        $settings['style_formats'] = json_encode( $style_formats );

        //var_dump($settings);

    return $settings;
} );

add_filter( 'wp_mce_translation', function( $mce_translation, $mce_locale ){

    $mce_translation['Formats'] = 'Formats & Styles';

    return $mce_translation;

}, 10, 2 );


add_action( 'wp_footer', function(){ ?>

<script type="text/javascript">

( function( $ ) {

    // Creating custom :external selector
    $.expr[':'].external = function(obj){
      return !obj.href.match(/^mailto\:/) && (obj.hostname !== location.hostname) && !obj.href.match(/^tel\:/);
    };
    $('a:external').addClass('external-link');

    var speedBump1 = "<?php the_field('speed_bump_level_1', 'options' ); ?>";
    var speedBump2 = "<?php the_field('speed_bump_level_2', 'options' ); ?>";
    var speedBump3 = "<?php the_field('speed_bump_level_3', 'options' ); ?>";

    $('.external-link').not('.outgoing').click( function( event ) {
        var url = $(this).attr('href');
        var message = '';
        if ( $(this).hasClass( 'external-lvl-1') || $(this).parent().hasClass( 'external-lvl-1') ){
            message = speedBump1;
        } else if ( $(this).hasClass( 'external-lvl-2') || $(this).parent().hasClass( 'external-lvl-2') ){
            message = speedBump2;
        } else if ( $(this).hasClass( 'external-lvl-3') || $(this).parent().hasClass( 'external-lvl-3') ){
            message = speedBump3;
        } else {
            return;
        }
        event.preventDefault();
        var confirm_box = confirm( message );
        if (confirm_box) {
            window.open( url );
        }
    } );

    // Assemble View on Google link for locations.
    $('#view-on-google').on('click', function(){

        var link = $( '.gm-style > div > a[href*="maps.google.com/maps?ll"]').attr( 'href' );
        
        var location = encodeURIComponent( 'Landmark National Bank ' +  $('.wpsl-location-address p').html().replace(/<br>/g, ' ').replace( /PO Box ([0-9]{1,4})/ , '' ).replace( / {2,}/, ' ' ) );

        var confirm_box_google = confirm( speedBump1 );

        if ( confirm_box_google ) {
            window.open( link + '&q=' + location );    
        }
        
    });

} )( jQuery );

</script>

<?php
});


add_filter( 'dashboard_glance_items', function( $items = array() ) {

    $post_types = array( 'location' );
    
    foreach( $post_types as $type ) {

        if( ! post_type_exists( $type ) ) continue;

        $num_posts = wp_count_posts( $type );
        
        if( $num_posts ) {
            
            $published = intval( $num_posts->publish );
            $post_type = get_post_type_object( $type );
            
            $text = _n( '%s ' . $post_type->labels->singular_name, '%s ' . $post_type->labels->name, $published, 'your_textdomain' );
            $text = sprintf( $text, number_format_i18n( $published ) );
            
            if ( current_user_can( $post_type->cap->edit_posts ) ) {
                $items[] = sprintf( '<a class="%1$s-count" href="edit.php?post_type=%1$s">%2$s</a>', $type, $text ) . "\n";
            } else {
                $items[] = sprintf( '<span class="%1$s-count">%2$s</span>', $type, $text ) . "\n";
            }
        }
    }
    
    return $items;

} , 10, 1 );


// Dequeue the flags since we aren't using them
if ( ! is_admin() ){
    add_action( 'init', function(){
        wp_dequeue_script( 'flags' );
    }, 11 );
}



