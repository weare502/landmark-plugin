<?php

class Landmark_Quick_Links extends WP_Widget {

	function __construct() {
			// Instantiate the parent object
			parent::__construct( 
				'landmark_quick_links', // Base ID
				__( 'Landmark: Quick Links', 'lnb' ), // Name
				array( 
					'description' => __( 'Quick Links for the Landmark National Bank website.', 'lnb' ), 
				)
			);
		}


	function widget( $args, $instance ) {

		echo $args['before_widget'];

		echo $args['before_title'] . __( 'Quick Links', 'lnb' ) . $args['after_title']; 


		$rewards = get_field('landmark_rewards', 'options' );
		$rewards_bump = '';
		if ( get_field( 'external_rewards_link', 'options' ) ){
			$rewards = get_field('landmark_rewards_external', 'options' );
			$rewards_bump = get_field('rewards_speed_bump', 'options' );
		}

		$checks = get_field('reorder_checks', 'options' );
		$checks_bump = '';
		if ( get_field( 'external_checks_link', 'options' ) ){
			$checks = get_field('reorder_checks_external', 'options' );
			$checks_bump = get_field('checks_speed_bump', 'options' );
		}

		$stocks = get_field('stock_quote', 'options' );
		$stock_bump = '';
		if ( get_field( 'external_stock_link', 'options' ) ){
			$stocks = get_field('stock_quote_external', 'options' );
			$stock_bump = get_field('stock_speed_bump', 'options' );
		}

		?>
		
		<ul>
			<li><a href="<?php echo $rewards; ?>" class=" <?php echo $rewards_bump; ?> "><span class="fa fa-money"></span> <span><?php _e( 'Landmark Rewards', 'lnb' ); ?></span></a></li>
			<li><a href="<?php the_field('financial_calculators', 'options' ); ?>"><span class="fa fa-calculator"></span><span> <?php _e( 'Financial Calculators', 'lnb' ); ?></span></a></li>
			<li><a href="<?php echo $checks; ?>" class=" <?php echo $checks_bump; ?> "><span class="fa fa-pencil"></span> <span><?php _e( 'Reorder Checks', 'lnb' ); ?></span></a></li>
			<li><a href="<?php the_field('lost_stolen_credit_card', 'options' ); ?>"><span class="fa fa-credit-card"></span> <span><?php _e( 'Lost/Stolen Debit Card', 'lnb' ); ?></span></a></li>
			<li><a href="<?php echo $stocks; ?>" class=" <?php echo $stock_bump; ?> "><span class="fa fa-line-chart"></span><span><?php _e( 'Stock Quote', 'lnb' ); ?></span></a></li>
		</ul>
		
		<?php

		echo $args['after_widget'];

	}


}

add_action( 'widgets_init', function() {

	register_widget( 'Landmark_Quick_Links' );

});